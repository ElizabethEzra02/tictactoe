public class PrintBoard {
    public static void printBoard(char [][] board) {
        for (char[] chars : board) {
            for(char c : chars) {
                System.out.print(c);
            }
            System.out.println();
        }}
}

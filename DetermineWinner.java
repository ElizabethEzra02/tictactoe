import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DetermineWinner {



    public static  String winner() {
        List<Integer> firstRow = Arrays.asList(1, 2, 3);
        List<Integer> secondRow = Arrays.asList(4, 5, 6);
        List<Integer> thirdRow = Arrays.asList(7, 8, 9);
        List<Integer> firstColumn = Arrays.asList(1, 4, 7);
        List<Integer> secondColumn = Arrays.asList(2, 5, 8);
        List<Integer> thirdColumn = Arrays.asList(3, 6, 9);
        List<Integer> firstDiagonal = Arrays.asList(1, 5, 9);
        List<Integer> secondDiagonal = Arrays.asList(7, 5, 3);

        List<List> winningConditions = new ArrayList<>();
        winningConditions.add(firstRow);
        winningConditions.add(secondRow);
        winningConditions.add(thirdRow);
        winningConditions.add(firstColumn);
        winningConditions.add(secondColumn);
        winningConditions.add(thirdColumn);
        winningConditions.add(firstDiagonal);
        winningConditions.add(secondDiagonal);

        for (List pos : winningConditions) {

        if (TicTacToe.playerPositions.containsAll(pos)) {
            return "Congratulations! You won!";
        } else if (TicTacToe.computerPositions.containsAll(pos)) {
            return "Computer wins! Better luck next time...";
        } else if (TicTacToe.playerPositions.size() + TicTacToe.computerPositions.size() == 9) {
            return "It's a tie!";}


    }
            return "";
}}

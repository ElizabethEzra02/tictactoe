import java.util.ArrayList;

public class Positions {


    public static void position(char [][] board,int position , String user){

        char piece = ' ';
        if (user.equals("player")){
            piece = 'X';
            TicTacToe.playerPositions.add(position);
            TicTacToe.takenPositions.add(position);
        }else if (user.equals("computer")){
            piece = 'O';
            TicTacToe.takenPositions.add(position);
            TicTacToe.computerPositions.add(position);}


        if (position == 1) {
            board[0][0] = piece;
        }
        else if (position == 2) {
            board[0][2] = piece;
        }
        else if (position == 3) {
            board[0][4] = piece;
        }
        else if (position == 4) {
            board[2][0] = piece;
        }
        else if (position == 5) {
            board[2][2] = piece;
        }
        else if (position == 6) {
            board[2][4] = piece;
        }
        else if (position == 7) {
            board[4][0] = piece;
        }
        else if (position == 8) {
            board[4][2] = piece;
        }
        else if (position == 9) {
            board[4][4]= piece;
        }
    }
}

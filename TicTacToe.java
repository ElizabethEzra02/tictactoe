import java.util.*;

public class TicTacToe {
    /*main code of the game*/
        static ArrayList<Integer> takenPositions = new ArrayList<>();
        static ArrayList<Integer> computerPositions = new ArrayList<>();
        static ArrayList<Integer> playerPositions = new ArrayList<>();


        public static void main(String[] args) {


            char [][] board = Gameboard.board;
            Scanner scan  = new Scanner((System.in));

            while(true){

                System.out.println("Your turn ... Choose a position (1-9): ");
                String playerPosition = String.valueOf(scan.nextInt());



                if ("off".equals(playerPosition)) {
                    System.out.println("Quitting...");
                    break;

                }
                while(takenPositions.contains(playerPosition)){
                    System.out.println("That position is taken... please try again.");
                    playerPosition= String.valueOf(scan.nextInt());
                }



                Positions.position(board, Integer.parseInt(playerPosition),"player");
                PrintBoard.printBoard(board);
                String result = DetermineWinner.winner();
                if (result.length() > 0){
                    System.out.println(result);
                    break;
                }

                Random random = new Random();
                int computerPosition = random.nextInt(9) +1;
                System.out.println("Computer's turn: " + computerPosition);
                while(takenPositions.contains(computerPosition)){
                    System.out.println("position was taken...retrying");
                    System.out.println("Computer's turn: " + computerPosition);
                    computerPosition = random.nextInt(9) +1;
                }

                Positions.position(board,computerPosition,"computer");
                PrintBoard.printBoard(board);
                result = DetermineWinner.winner() ;
                if (result.length() > 0){
                    System.out.println(result);
                    break;
                }
            }
        }}

